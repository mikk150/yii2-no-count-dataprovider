<?php

namespace mikk150\nocount\assets;

use yii\web\AssetBundle;

class FormPagerAsset extends AssetBundle
{
    public $sourcePath = '@mikk150/nocount/assets/dist';
    public $js = [
        'formPager.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
