;(function($){
    $.fn.mikk150FormPager = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist in jQuery.mikk150FormPager');
            return false;
        }
    };

    var defaults = {
        paginationUrl: undefined,
        paginationSelector: undefined,
        goToPageOnFocusOut: true
    };

    var paginationData = {};

    var methods = {
        init: function (options) {
            return this.each(function () {
                var $e = $(this);
                var settings = $.extend({}, defaults, options || {});
                var id = $e.attr('id');
                if (paginationData[id] === undefined) {
                    paginationData[id] = {};
                }

                paginationData[id] = $.extend(paginationData[id], { settings: settings });

                var paginationEvents = 'change keydown';
                var enterPressed = false;

                initEventHandler($e, 'paginate', paginationEvents, settings.paginationSelector, function (event) {
                    if (event.type === 'keydown') {
                        if (event.keyCode !== 13) {
                            return; // only react to enter key
                        } else {
                            enterPressed = true;
                        }
                    } else {
                        // prevent processing for both keydown and change events
                        if (enterPressed) {
                            enterPressed = false;
                            return;
                        }
                    }
                    if (!settings.goToPageOnFocusOut && event.type !== 'keydown') {
                        return false;
                    }

                    methods.applyPagination.apply($e);

                    return false;
                });
            });
        },
        applyPagination: function($e) {
            var $formPager = $(this);
            var settings = paginationData[$formPager.attr('id')].settings;
            var data = {};

            $.each($(settings.paginationSelector).serializeArray(), function () {
                if (!(this.name in data)) {
                    data[this.name] = [];
                }
                data[this.name].push(this.value);
            });

            var namesInFilter = Object.keys(data);

            $.each(yii.getQueryParams(settings.paginationUrl), function (name, value) {
                if (namesInFilter.indexOf(name) === -1 && namesInFilter.indexOf(name.replace(/\[\d*\]$/, '')) === -1) {
                    if (!$.isArray(value)) {
                        value = [value];
                    }
                    if (!(name in data)) {
                        data[name] = value;
                    } else {
                        $.each(value, function (i, val) {
                            if ($.inArray(val, data[name])) {
                                data[name].push(val);
                            }
                        });
                    }
                }
            });

            var pos = settings.paginationUrl.indexOf('?');
            var url = pos < 0 ? settings.paginationUrl : settings.paginationUrl.substring(0, pos);
            var hashPos = settings.paginationUrl.indexOf('#');
            if (pos >= 0 && hashPos >= 0) {
                url += settings.paginationUrl.substring(hashPos);
            }

            $formPager.find('form.gridview-sort-form').remove();
            var $form = $('<form/>', {
                action: url,
                method: 'get',
                'class': 'gridview-sort-form',
                style: 'display:none',
                'data-pjax': ''
            }).appendTo($formPager);
            $.each(data, function (name, values) {
                $.each(values, function (index, value) {
                    $form.append($('<input/>').attr({ type: 'hidden', name: name, value: value }));
                });
            });

            $form.submit();
        }
    };

    var paginationEventHandlers = {};

    function initEventHandler($formPager, type, event, selector, callback) {
        var id = $formPager.attr('id');
        var prevHandler = paginationEventHandlers[id];
        if (prevHandler !== undefined && prevHandler[type] !== undefined) {
            var data = prevHandler[type];
            $(document).off(data.event, data.selector);
        }
        if (prevHandler === undefined) {
            paginationEventHandlers[id] = {};
        }
        $(document).on(event, selector, callback);
        paginationEventHandlers[id][type] = { event: event, selector: selector };
    }
})(window.jQuery);