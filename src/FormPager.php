<?php

namespace mikk150\nocount;

use mikk150\nocount\assets\FormPagerAsset;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\i18n\PhpMessageSource;
use yii\web\Request;

class FormPager extends Widget
{
    /**
     * @var Pagination the pagination object that this pager is associated with.
     * You must set this property in order to make LinkPager work.
     */
    public $pagination;
    /**
     * @var array HTML attributes for the pager container tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = ['class' => 'pagination form-inline'];

    public $absolute = false;

    public $pageInputOptions = ['class' => 'form-control'];

    public $pageInputFormGroupOptions = ['class' => 'form-group'];

    public $pageInputFormLabelOptions = [];

    public $pageSizeInputOptions = ['class' => 'form-control'];

    public $pageSizeInputFormGroupOptions = ['class' => 'form-group'];

    public $pageSizeInputFormLabelOptions = [];

    /**
     * @var string additional jQuery selector for selecting pagination input fields
     */
    public $paginationSelector;

    /**
     * @var string the layout that determines how different sections of the list view should be organized.
     * The following tokens will be replaced with the corresponding section contents:
     *
     * - `{summary}`: the summary section. See [[renderSummary()]].
     * - `{items}`: the list items. See [[renderItems()]].
     * - `{sorter}`: the sorter. See [[renderSorter()]].
     * - `{pager}`: the pager. See [[renderPager()]].
     */
    public $layout = "{pageInput}\n{sizeInput}";

    /**
     * Initializes the pager.
     */
    public function init()
    {
        parent::init();

        if ($this->pagination === null) {
            throw new InvalidConfigException('The "pagination" property must be set.');
        }

        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }

        if (!isset($this->pageInputOptions['id'])) {
            $this->pageInputOptions['id'] = $this->getId() . '-' . $this->pagination->pageParam;
        }

        if (!isset($this->pageSizeInputOptions['id'])) {
            $this->pageSizeInputOptions['id'] = $this->getId() . '-' . $this->pagination->pageSizeParam;
        }

        if (Yii::$app && !isset(Yii::$app->i18n->translations['mikk150/nocount/*'])) {
            Yii::$app->i18n->translations['mikk150/nocount/*'] = [
                'class' => PhpMessageSource::className(),
                'sourceLanguage' => 'en-US',
                'basePath' => '@mikk150/nocount/messages',
                'fileMap' => [
                    'mikk150/nocount/formpager' => 'formpager.php',
                    'mikk150/nocount/grid' => 'grid.php',
                ],
            ];
        }
    }

    public function registerAsset()
    {
        FormPagerAsset::register($this->view);
    }

    /**
     * Executes the widget.
     * This overrides the parent implementation by displaying the generated pagination form.
     */
    public function run()
    {
        $view = $this->getView();
        FormPagerAsset::register($view);
        $id = $this->options['id'];
        $options = Json::htmlEncode(array_merge($this->getClientOptions()));
        $view->registerJs("jQuery('#$id').mikk150FormPager($options);");

        $this->registerAsset();

        echo $this->renderPageForm();
    }

    /**
     * Returns the options for the grid view JS widget.
     * @return array the options
     */
    protected function getClientOptions()
    {
        $id = $this->options['id'];
        $paginationSelector = "#$id input, #$id select";
        if (isset($this->paginationSelector)) {
            $paginationSelector .= ', ' . $this->paginationSelector;
        }

        return [
            'paginationUrl' => $this->getFormUrl(),
            'paginationSelector' => $paginationSelector,
        ];
    }

    public function renderPageForm()
    {
        $content = preg_replace_callback('/{\\w+}/', function ($matches) {
            $content = $this->renderSection($matches[0]);

            return $content === false ? $matches[0] : $content;
        }, $this->layout);

        $options = $this->options;
        $tag = ArrayHelper::remove($options, 'tag', 'div');
        return Html::tag($tag, $content, $options);
    }


    /**
     * Renders a section of the specified name.
     * If the named section is not supported, false will be returned.
     * @param string $name the section name, e.g., `{summary}`, `{items}`.
     * @return string|bool the rendering result of the section, or false if the named section is not supported.
     */
    public function renderSection($name)
    {
        switch ($name) {
            case '{pageInput}':
                return $this->renderPageInput();
            case '{sizeInput}':
                return $this->renderPageSizeInput();
            default:
                return false;
        }
    }

    public function renderPageInput()
    {
        $inputName = $this->pagination->pageParam;
        return Html::tag(
            'div', 
                Html::tag('label', Yii::t('mikk150/nocount/formpager', 'Page: '), ArrayHelper::merge(
                    $this->pageInputFormLabelOptions,
                    ['for' => $this->pageInputOptions['id']]
                )).
                Html::input('input', $inputName, $this->pagination->getPage() + 1, $this->pageInputOptions),
            $this->pageInputFormGroupOptions
        );
    }

    public function renderPageSizeInput()
    {
        $inputName = $this->pagination->pageSizeParam;
        return Html::tag(
            'div',
            Html::tag('label', Yii::t('mikk150/nocount/formpager', 'Page size: '), ArrayHelper::merge(
                $this->pageSizeInputFormLabelOptions,
                ['for' => $this->pageSizeInputOptions['id']]
            )).
                Html::input('input', $inputName, $this->pagination->getPageSize(), $this->pageSizeInputOptions),
            $this->pageSizeInputFormGroupOptions
        );
    }

    private function getFormUrl()
    {
        if (($params = $this->pagination->params) === null) {
            $request = Yii::$app->getRequest();
            $params = $request instanceof Request ? $request->getQueryParams() : [];
        }
        unset($params[$this->pagination->pageParam]);
        unset($params[$this->pagination->pageSizeParam]);

        $params[0] = $this->pagination->route === null ? Yii::$app->controller->getRoute() : $this->pagination->route;
        $urlManager = $this->pagination->urlManager === null ? Yii::$app->getUrlManager() : $this->pagination->urlManager;
        if ($this->absolute) {
            return $urlManager->createAbsoluteUrl($params);
        }

        return $urlManager->createUrl($params);
    }
}