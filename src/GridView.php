<?php

namespace mikk150\nocount;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\i18n\PhpMessageSource;

class GridView extends \yii\grid\GridView
{
    public function init()
    {
        parent::init();

        if (Yii::$app && !isset(Yii::$app->i18n->translations['mikk150/nocount/*'])) {
            Yii::$app->i18n->translations['mikk150/nocount/*'] = [
                'class' => PhpMessageSource::className(),
                'sourceLanguage' => 'en-US',
                'basePath' => '@mikk150/nocount/messages',
                'fileMap' => [
                    'mikk150/nocount/formpager' => 'formpager.php',
                    'mikk150/nocount/grid' => 'grid.php',
                ],
            ];
        }
    }

    /**
     * Renders the summary text.
     */
    public function renderSummary()
    {
        $count = $this->dataProvider->getCount();
        if ($count <= 0) {
            return '';
        }

        $summaryOptions = $this->summaryOptions;
        $tag = ArrayHelper::remove($summaryOptions, 'tag', 'div');

        if (($pagination = $this->dataProvider->getPagination()) !== false) {
            $begin = $pagination->getPage() * $pagination->pageSize + 1;
            $end = $begin + $count - 1;
            $page = $pagination->getPage() + 1;

            if (($summaryContent = $this->summary) === null) {
                return Html::tag($tag, Yii::t('mikk150/nocount/grid', 'Showing <b>{begin, number}-{end, number}</b>.', [
                    'begin' => $begin,
                    'end' => $end,
                    'count' => $count,
                    'page' => $page,
                ]), $summaryOptions);
            }
        } else {
            $begin = $page = 1;
            $end = $count;
            if (($summaryContent = $this->summary) === null) {
                return Html::tag($tag, Yii::t('mikk150/nocount/grid', 'Total <b>{count, number}</b> {count, plural, one{item} other{items}}.', [
                    'begin' => $begin,
                    'end' => $end,
                    'count' => $count,
                    'page' => $page,
                ]), $summaryOptions);
            }
        }

        return Yii::$app->getI18n()->format($summaryContent, [
            'begin' => $begin,
            'end' => $end,
            'count' => $count,
            'page' => $page,
        ], Yii::$app->language);
    }

    /*
     * Renders the pager.
     * @return string the rendering result
     */
    public function renderPager()
    {
        $pagination = $this->dataProvider->getPagination();
        if ($pagination === false || $this->dataProvider->getCount() <= 0) {
            return '';
        }
        /* @var $class LinkPager */
        $pager = $this->pager;
        $class = ArrayHelper::remove($pager, 'class', FormPager::className());
        $pager['pagination'] = $pagination;
        $pager['view'] = $this->getView();

        return $class::widget($pager);
    }
}