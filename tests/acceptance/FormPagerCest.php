<?php

namespace tests\acceptance;

use Facebook\WebDriver\WebDriverKeys;
use tests\AcceptanceTester;

class FormPagerCest
{
    public function testFirstPageWorks(AcceptanceTester $I)
    {
        $I->amOnPage('?r=test/list');
        $I->see('Element 1', 'table');
        $I->see('Element 20', 'table');
        $I->dontSee('Element 21', 'table');
    }

    public function testGoingToSecondPage(AcceptanceTester $I)
    {
        $I->amOnPage('?r=test/list');
        $I->pressKey('//input[@name="page"]', WebDriverKeys::BACKSPACE);
        $I->pressKey('//input[@name="page"]', '2');
        $I->pressKey('//input[@name="page"]', WebDriverKeys::ENTER);

        $I->wait(1);

        $I->dontSee('Element 1', 'table');
        $I->dontSee('Element 20', 'table');
        $I->see('Element 21', 'table');
        $I->see('Element 40', 'table');
        $I->dontSee('Element 41', 'table');
    }

    public function testBlurringPageFieldGoesToPage(AcceptanceTester $I)
    {
        $I->amOnPage('?r=test/list');
        $I->pressKey('//input[@name="page"]', WebDriverKeys::BACKSPACE);
        $I->pressKey('//input[@name="page"]', '2');
        $I->clickWithLeftButton('//body');

        $I->wait(1);

        $I->dontSee('Element 1', 'table');
        $I->dontSee('Element 20', 'table');
        $I->see('Element 21', 'table');
        $I->see('Element 40', 'table');
        $I->dontSee('Element 41', 'table');
    }

    public function testChangingPageRetainsGetParameters(AcceptanceTester $I)
    {
        $I->amOnPage('?r=test/list&awesome=keepme');
        $I->pressKey('//input[@name="page"]', WebDriverKeys::BACKSPACE);
        $I->pressKey('//input[@name="page"]', '2');
        $I->clickWithLeftButton('//body');

        $I->wait(1);

        $I->dontSee('Element 1', 'table');
        $I->dontSee('Element 20', 'table');
        $I->see('Element 21', 'table');
        $I->see('Element 40', 'table');
        $I->dontSee('Element 41', 'table');

        $I->seeInCurrentUrl('awesome=keepme');
    }
}