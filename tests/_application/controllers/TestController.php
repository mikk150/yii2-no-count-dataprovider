<?php

namespace yiiacceptance\controllers;

use mikk150\nocount\data\ArrayDataProvider;
use yii\web\Controller;

/**
*
*/
class TestController extends Controller
{
    public function actionList()
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => array_map(function ($element) {
                return ['test' => 'Element ' . $element];
            }, range(1,1000))
        ]);

        return $this->render('list', [
            'dataProvider' => $dataProvider
        ]);
    }
}
