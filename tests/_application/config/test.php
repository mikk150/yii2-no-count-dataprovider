<?php

return [
    'id' => 'test',
    'basePath' => dirname(__DIR__),
    'defaultRoute' => 'test/index',
    'aliases' => [
        '@bower' => __DIR__ . '/../../../vendor/bower-asset',
        '@runtime' => dirname(dirname(__DIR__)) . '/_output'
    ],
    'bootstrap' => ['log'],
    'controllerNamespace' => 'yiiacceptance\controllers',
    'components' => [
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'assetManager' => [
            'forceCopy' => true,
        ],
        'request' => [
            'enableCookieValidation' => false
        ]
    ]
];
