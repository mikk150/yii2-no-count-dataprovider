<?php
require(__DIR__ . '/../../../vendor/autoload.php');
require(__DIR__ . '/../../../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../../../c3.php');

Yii::setAlias('yiiacceptance', dirname(__DIR__));
Yii::setAlias('@mikk150/nocount', dirname(dirname(dirname(__DIR__))) . '/src');

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

// instantiate and configure the application
(new yii\web\Application(require(dirname(__DIR__) . '/config/test.php')))->run();
