<?php

namespace tests\unit\rest;

use mikk150\nocount\data\ArrayDataProvider;
use mikk150\nocount\rest\Serializer;
use tests\unit\TestCase;
use Yii;
use yii\rest\Controller;
use yii\web\Request;
use yii\web\Response;

class SerializerTest extends TestCase
{
    public function testNotAddingCountHeaders()
    {
        $this->mockApplication([
            'components' => [
                'request' => [
                    'scriptUrl' => '',
                ],
            ]
        ], \yii\web\Application::class);

        Yii::$app->controller = new Controller('huinjaa', Yii::$app);

        $response = new Response();
        
        $serializer = new Serializer([
            'response' => $response,
            'request' => new Request(),
        ]);

        $dataProvider = new ArrayDataProvider();

        $serializer->serialize($dataProvider);

        $this->assertTrue($response->headers->has('x-pagination-current-page'));
        $this->assertTrue($response->headers->has('x-pagination-per-page'));
        $this->assertFalse($response->headers->has('x-pagination-total-count'));
        $this->assertFalse($response->headers->has('x-pagination-page-count'));
    }

    public function testNotAddingCountToEnvelope()
    {
        $this->mockApplication([
            'components' => [
                'request' => [
                    'scriptUrl' => '',
                ],
            ]
        ], \yii\web\Application::class);

        Yii::$app->controller = new Controller('huinjaa', Yii::$app);

        $response = new Response();
        
        $serializer = new Serializer([
            'collectionEnvelope' => 'collection',
            'response' => $response,
            'request' => new Request(),
        ]);

        $dataProvider = new ArrayDataProvider([
            'allModels' => [
                [
                    'field1' => 'test'
                ]
            ]
        ]);

        $data = $serializer->serialize($dataProvider);

        $this->assertArrayHasKey('_meta', $data);
        
        $this->assertArrayHasKey('currentPage', $data['_meta']);
        $this->assertArrayHasKey('perPage', $data['_meta']);
        $this->assertArrayNotHasKey('totalCount', $data['_meta']);
        $this->assertArrayNotHasKey('pageCount', $data['_meta']);
    }
}
