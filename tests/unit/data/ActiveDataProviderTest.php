<?php

namespace tests\unit\data;

use Codeception\Stub\Expected;
use mikk150\nocount\data\ActiveDataProvider;
use mikk150\nocount\data\Pagination;
use tests\unit\TestCase;
use yii\base\Component;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\data\Pagination as DataPagination;
use yii\db\Connection;
use yii\db\Query;

class ActiveDataProviderTest extends TestCase
{
    public function testCountIsNeverExecuted()
    {
        $this->mockApplication([
            'components' => [
                'request' => [
                    'scriptUrl' => __FILE__
                ],
            ]
        ], \yii\web\Application::class);
        
        $dbConnection = $this->construct(Connection::class, [[
            'dsn' => 'sqlite::memory:'
        ]]);

        $query = $this->construct(Query::class, [], [
            'count' => Expected::never(),
            'all' => Expected::once([])
        ]);

        $activeDataProvider = new ActiveDataProvider([
            'db' => $dbConnection,
            'query' => $query,
        ]);

        $activeDataProvider->getModels();
    }

    public function testGivingRandomObject()
    {
        $this->expectException(InvalidConfigException::class);
        $this->expectExceptionMessage('The "query" property must be an instance of a class that implements the QueryInterface e.g. yii\db\Query or its subclasses.');

        $activeDataProvider = new ActiveDataProvider([
            'query' => new Component()
        ]);

        $activeDataProvider->getModels();
    }

    public function testExplicitlyGivingPaginationObject()
    {
        $dbConnection = $this->construct(Connection::class, [[
            'dsn' => 'sqlite::memory:'
        ]]);

        $query = $this->construct(Query::class, [], [
            'count' => Expected::once(0),
            'all' => Expected::once([])
        ]);

        $activeDataProvider = new ActiveDataProvider([
            'db' => $dbConnection,
            'query' => $query,
            'pagination' => new DataPagination(),
        ]);

        $activeDataProvider->getModels();
    }

    public function testExplicitlyGivingCorrectPaginationObject()
    {
        $dbConnection = $this->construct(Connection::class, [[
            'dsn' => 'sqlite::memory:'
        ]]);

        $query = $this->construct(Query::class, [], [
            'count' => Expected::never(),
            'all' => Expected::once([])
        ]);

        $activeDataProvider = new ActiveDataProvider([
            'db' => $dbConnection,
            'query' => $query,
            'pagination' => new Pagination(),
        ]);

        $activeDataProvider->getModels();
    }

    public function testExplicitlyGivingFalseAsPagination()
    {
        $dbConnection = $this->construct(Connection::class, [[
            'dsn' => 'sqlite::memory:'
        ]]);

        $query = $this->construct(Query::class, [], [
            'count' => Expected::never(),
            'all' => Expected::once([])
        ]);

        $activeDataProvider = new ActiveDataProvider([
            'db' => $dbConnection,
            'query' => $query,
            'pagination' => false,
        ]);

        $activeDataProvider->getModels();
    }

    public function testExplicitlyGivingNotPaginationObjectAsPagination()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Only Pagination instance, configuration array or false is allowed.');

        $dbConnection = $this->construct(Connection::class, [[
            'dsn' => 'sqlite::memory:'
        ]]);

        $query = $this->construct(Query::class, [], [
        ]);

        $activeDataProvider = new ActiveDataProvider([
            'db' => $dbConnection,
            'query' => $query,
            'pagination' => new Component(),
        ]);

        $activeDataProvider->getModels();
    }
}
