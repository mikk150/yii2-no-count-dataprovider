<?php

namespace tests\unit;

use Codeception\Stub\Expected;
use mikk150\nocount\data\ArrayDataProvider;
use mikk150\nocount\GridView;
use yii\data\Pagination;
use yii\data\Sort;
use yii\i18n\Formatter;
use yii\web\AssetManager;
use yii\web\UrlManager;
use yii\web\View;

class GridViewTest extends TestCase
{
    public function testCountNeverExecuted()
    {
        $dataProvider = $this->construct(ArrayDataProvider::class, [[
            'pagination' => [
                'class' => Pagination::class,
                'route' => 'test/test',
                'urlManager' => $this->construct(UrlManager::class,[[
                    'scriptUrl' => '/',
                ]]),
                'params' => [
                    'page' => 0
                ],
            ],
            'sort' => [
                'class' => Sort::class,
                'params' => [],
            ],
            'allModels' => array_map(function ($element) {
                return [
                    'test' => 'test' . $element
                ];
            }, range(1,1000))
        ]], [
            'getTotalCount' => Expected::never(),
        ]);

        $widget = GridView::widget([
            'filterUrl' => '/',
            'formatter' => [
                'class' => Formatter::class,
                'timeZone' => 'UTC',
                'locale' => 'en'
            ],
            'view' => $this->make(View::class, [
                'assetManager' => $this->make(AssetManager::class, [
                    'publish' => true
                ])
            ]),
            'dataProvider' => $dataProvider,
        ]);

        $this->assertEqualsWithoutLE('<div id="w0" class="grid-view"><div class="summary">Showing <b>1-20</b>.</div>
<table class="table table-striped table-bordered"><thead>
<tr><th>Test</th></tr>
</thead>
<tbody>
<tr data-key="0"><td>test1</td></tr>
<tr data-key="1"><td>test2</td></tr>
<tr data-key="2"><td>test3</td></tr>
<tr data-key="3"><td>test4</td></tr>
<tr data-key="4"><td>test5</td></tr>
<tr data-key="5"><td>test6</td></tr>
<tr data-key="6"><td>test7</td></tr>
<tr data-key="7"><td>test8</td></tr>
<tr data-key="8"><td>test9</td></tr>
<tr data-key="9"><td>test10</td></tr>
<tr data-key="10"><td>test11</td></tr>
<tr data-key="11"><td>test12</td></tr>
<tr data-key="12"><td>test13</td></tr>
<tr data-key="13"><td>test14</td></tr>
<tr data-key="14"><td>test15</td></tr>
<tr data-key="15"><td>test16</td></tr>
<tr data-key="16"><td>test17</td></tr>
<tr data-key="17"><td>test18</td></tr>
<tr data-key="18"><td>test19</td></tr>
<tr data-key="19"><td>test20</td></tr>
</tbody></table>
<div id="w1" class="pagination form-inline"><div class="form-group"><label for="w1-page">Page: </label><input type="input" id="w1-page" class="form-control" name="page" value="1"></div>
<div class="form-group"><label for="w1-per-page">Page size: </label><input type="input" id="w1-per-page" class="form-control" name="per-page" value="20"></div></div></div>', $widget);
    }
}
